using UnityEngine;

public class ThingTimer : MonoBehaviour {
    public int delay;
    private float triggerTime;
    private float last;
    public Color idleColor;
    public Color activeColor;
    public Color readyColor;
    private Properties props;
    public bool waitForReadyInteraction;

    void Awake() {
        props = GetComponent<Properties>();
        Debug.Log("Thing timer is awake for " + gameObject.name);
        triggerTime = Time.time;
        SetColor(idleColor);
    }

    void Update() {
        // Debug.Log("Updating Thing Timer");
        if (props.triggered && !props.active) {
            ActivateThing();
            return;
        }
        if (props.active) {
            CheckTime();
        }
    }

    public void ActivateThing() {
        props.ready = false;
        props.idle = false;
        props.active = true;
        triggerTime = Time.time;
        SetColor(activeColor);
    }

    void CheckTime() {
        last = Time.time;
        float elapsed = last - triggerTime;
        bool hasElapsed = elapsed > delay;
        if (props.active && !props.ready && hasElapsed) {
            SetReady();
        }
    }

    private void SetReady() {
        Debug.Log(gameObject.name + " is ready after " + delay + " seconds!");
        props.ready = true;
        SetColor(readyColor);
        if (!waitForReadyInteraction) {
            SetIdle();
        }
    }

    public void SetIdle() {
        props.idle = true;
        props.triggered = false;
        props.active = false;
        props.ready = false;
        SetColor(idleColor);
    }

    public bool GetState() {
        return props.ready;
    }

    public void TriggerThing() {
        if (props.triggered) {
            return;
        }
        triggerTime = Time.time;
        props.triggered = true;
    }

    public void ResetThing() {
        props.triggered = false;
        props.idle = true;
    }

    void SetColor(Color color) {
        // // Fetch the Renderer from the GameObject
        Renderer rend = GetComponent<Renderer>();
        // // Set the main Color of the Material
        rend.material.shader = Shader.Find("_Color");
        rend.material.SetColor("_Color", color);
        // // Find the Specular shader and change its Color
        // // need this so it's not just pink
        rend.material.shader = Shader.Find("Specular");
        rend.material.SetColor("_SpecColor", Color.clear);
    }
}
