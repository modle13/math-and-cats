﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    public static Player player;
    private Properties props;
    private Rigidbody rigidBody;
    private float speed = 10f;
    float horizontal = 0;
    float vertical = 0;
    public float xUnit = 2.0f;
    public float yUnit = 2.0f;
    int brakeMultiplier = 4;
    Inventory inventory;
    float lastTrigger;
    float delay = 0.2f;
    GameObject scoreBoard;
    public bool debug = false;

    public LayerMask blockingLayer;

    void Awake() {
        // singleton pattern
        if (player == null) {
            DontDestroyOnLoad(gameObject);
            player = this;
        } else if (player != this) {
            Destroy(gameObject);
        }
    }

    void Start () {
        SetInitialReferences();
    }

    void SetInitialReferences() {
        props = GetComponent<Properties>();
        rigidBody = GetComponent<Rigidbody>();
        inventory = GetComponent<Inventory>();
        scoreBoard = transform.Find("Display").gameObject;
    }

    void Update () {
        Move();
        SetDisplay();
    }

    void Move() {
        GetCoordinates();
        MoveSprite();
    }

    void GetCoordinates() {
        horizontal = Input.GetAxis("Horizontal");
		vertical = Input.GetAxis("Vertical");
    }

    void MoveSprite() {
        // FIXME: this is pretty rough; player becomes hard to control after holding keys for any significant period of time
        Vector2 moveForce;
        if (horizontal != 0 || vertical != 0) {
            moveForce = new Vector2(horizontal * speed, vertical * speed);
        } else {
            Vector2 velocity = rigidBody.velocity;
            moveForce = new Vector2(-rigidBody.velocity.x * brakeMultiplier, -rigidBody.velocity.y * brakeMultiplier);
        }
        rigidBody.AddForce(moveForce);
    }

    int CheckCycle() {
        // controller needs to be in X-input mode for this to actually be triggers
        int val = 0;
        float elapsed = Time.time - lastTrigger;
        bool hasElapsed = elapsed > delay;
        /*
         * Mathf.RoundToInt() doesn't quite do what we want, so we have to roll our own
         * Xinput mode seems to return ints, but if the controller is connected
         * and not in use, but is in Dinput mode, it will return a float
         * -0.5~-1.0 will return -1, -0.49~0.49 will return 0, 0.5~1.0 will return 1
         * but we want it to always return 0 unless it's -1 or 1
         */
        int triggerVal = CheckTriggerAxis();
        if (triggerVal != 0 && hasElapsed) {
            Debug.Log("Trigger detected: " + Input.GetAxis("Triggers") + " converted to " + triggerVal);
            val = triggerVal;
            lastTrigger = Time.time;
        } else if (Input.GetKeyDown("q")) {
            Debug.Log("Q key detected; selecting previous");
            val = -1;
        } else if (Input.GetKeyDown("e")) {
            Debug.Log("E key detected, selecting next");
            val = 1;
        } else if (Input.GetKeyDown("f")) {
            Debug.Log("F key detected; putiing thing in");
            if (props.target != null) {
                Debug.Log("target is " + props.target.name);
                props.target.GetComponent<ActionProcessor>().ProcessAction("add");
            } else {
                Debug.Log("player has no target");
            }
        } else if (Input.GetKeyDown("r")) {
            Debug.Log("R key detected, taking thing out");
            if (props.target != null) {
                Debug.Log("target is " + props.target.name);
                props.target.GetComponent<ActionProcessor>().ProcessAction("remove");
            } else {
                Debug.Log("player has no target");
            }
        }
        return val;
    }

    int CheckTriggerAxis() {
        float triggerVal = Input.GetAxis("Triggers");
        if (triggerVal > -1.0 && triggerVal < 1.0) {
            return 0;
        }
        return Mathf.RoundToInt(triggerVal);
    }

    void SetDisplay() {
        int cycle = CheckCycle();
        if (cycle > 0) {
            inventory.SelectNext();
        } else if (cycle < 0) {
            inventory.SelectPrevious();
        }
        int currentValue = inventory.GetCurrent();
        scoreBoard.GetComponent<Properties>().UpdateValue(currentValue);
    }

    public void Pickup(int val) {
        inventory.Add(val);
        inventory.Show();
    }

    void OnGUI() {
        if (debug) {
            ShowCoordinates();
        }
    }

    void ShowCoordinates() {
        GUIStyle font = new GUIStyle();
        font.normal.textColor = Color.black;
        font.fontSize = (int)(xUnit * 10);
        GUI.Label(
            new Rect(Screen.width / 2, Screen.height / 2, 20, 20),
            GetText(horizontal, vertical),
            font
        );
        GUI.Label(
            new Rect(Screen.width / 2, Screen.height / 2 - 20, 20, 20),
            GetText(transform.position.x, transform.position.y),
            font
        );
    }

    string GetText(float x, float y) {
        return string.Format("x: {0:R}, y: {1:R}", x, y);
    }
}
