using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ObstacleCollision : MonoBehaviour {

    Properties props;

    public void Awake() {
        props = GetComponent<Properties>();
    }

    void OnCollisionEnter(Collision other) {
        if (other.gameObject.name == "Player") {
            // setText();
            // dead code?
            // if (props.type == "producer") {
            //     HandleProducer();
            // } else 
            if (props.type == "pickup") {
                HandlePickup(other.gameObject);
            } else if (props.type == "score") {
                HandleScore(other.gameObject);
            } else {
                SetColor();
            }
        }
    }

    void setText() {
        ObstacleTextMesh textMesh = GetComponent<ObstacleTextMesh>();
        // this is returning null; why?
        TextMeshPro tmproobj = textMesh.GetComponent<TextMeshPro>();
        tmproobj.gameObject.SetActive(true);
       // instantiate some text
    }

    // dead code?
    void HandleProducer() {
        Debug.Log("Hit a producer: " + gameObject.name);
        if (GetComponent<ThingTimer>().GetState()) {
            Debug.Log("Got thing");
            GetComponent<ThingTimer>().ResetThing();
        }
    }

    void HandlePickup(GameObject other) {
        other.GetComponent<Player>().Pickup(props.value);
        Destroy(gameObject);
    }

    void HandleScore(GameObject other) {
        GameObject scoreCounter = GameObject.Find("Score");
        scoreCounter.GetComponent<Properties>().AddToValue(1);
        Destroy(gameObject);
    }

    void SetColor() {
        //Fetch the Renderer from the GameObject
        Renderer rend = GetComponent<Renderer>();
        // Set the main Color of the Material to green
        rend.material.shader = Shader.Find("_Color");
        rend.material.SetColor("_Color", Color.green);
        // // Find the Specular shader and change its Color to red
        // // need this so it's not just pink
        // rend.material.shader = Shader.Find("Specular");
        // rend.material.SetColor("_SpecColor", Color.red);
    }
}
