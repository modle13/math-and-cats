using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleBucket : MonoBehaviour {
    public static ObstacleBucket bucket;
    private int maxObstacles = 50;
    public bool spawned = false;
    private float spawnRangeX;
    private float spawnRangeY;
    private float spawnCenterX;
    private float spawnCenterY;
    // private List<Object> spritesToInstantiate = new List<Object>();
    private int spawnDivisor = 24;
    public List<string> obstaclePrefabs = new List<string>() {
        "obstacle_0",
        "obstacle_1",
        "obstacle_2",
        "pickup_0"
    };
    Object numberSprite;
    Object matchSprite;

    void Awake() {
        // singleton pattern
        if (bucket == null) {
            bucket = this;
        } else if (bucket != this) {
            Destroy(gameObject);
        }
    }

    void Start() {
        GetSpawnRange();
        LoadPrefabs();
        Invoke("SpawnObstacles", 0.1f);
    }

    void GetSpawnRange() {
        // there has to be a better way to get these inside the walls; add them to the walls gameobject?
        Transform wallsTransform = GameObject.Find("Walls").transform;
        Transform canvasTransform = GameObject.Find("Canvas").transform;
        float spawnMaxX = 0;
        float spawnMinX = 0;
        float spawnMaxY = 0;
        float spawnMinY = 0;
        foreach (Transform child in wallsTransform) {
            if (spawnMaxX == 0) {
                spawnMaxX = wallsTransform.position.x + child.position.x;
                spawnMinX = wallsTransform.position.x + child.position.x;
                spawnMaxY = wallsTransform.position.y + child.position.y;
                spawnMinY = wallsTransform.position.y + child.position.y;
            }
            Debug.Log(child.name);
            if (spawnMinX > child.position.x) {
                spawnMinX = wallsTransform.position.x + child.position.x;
            }
            if (spawnMaxX < child.position.x) {
                spawnMaxX = wallsTransform.position.x + child.position.x;
            }
            if (spawnMinY > child.position.y) {
                spawnMinY = wallsTransform.position.y + child.position.y;
            }
            if (spawnMaxY < child.position.y) {
                spawnMaxY = wallsTransform.position.y + child.position.y;
            }
        }

        spawnRangeX = spawnMaxX - spawnMinX - 1f;
        spawnRangeY = spawnMaxY - spawnMinY;

        // having to do adjustments like this is awful
        spawnCenterX = (spawnMaxX / 2) + 2f;
        spawnCenterY = (spawnMaxY / 2) - 6f;
    }

    void LoadPrefabs() {
        numberSprite = Resources.Load("Prefabs/pickup_0", typeof(GameObject));
        matchSprite = Resources.Load("Prefabs/cat", typeof(GameObject));
    }

    void SpawnObstacles() {
        // foreach (string prefab in obstaclePrefabs) {
        //     spritesToInstantiate.Add(Resources.Load("Prefabs/pickup_0" + prefab, typeof(GameObject)));
        // }
        // obstacles = GameObject.Find("GameScripts/ObstacleBucket").transform;
        // Object targetSprite = spritesToInstantiate[0];

        while (transform.childCount < maxObstacles) {
            // int remainder = obstacles.childCount % spritesToInstantiate.Count;
            // targetSprite = spritesToInstantiate[remainder];

            SpawnNew();
        }
        Debug.Log("spawned " + transform.childCount + " obstacles");
        spawned = true;
    }

    GameObject SpawnNew() {
        float randomX = Random.Range(spawnCenterX - spawnRangeX, spawnCenterX + spawnRangeX);
        float randomY = Random.Range(spawnCenterY - spawnRangeY, spawnCenterY + spawnRangeY);
        Vector3 spritePosition = (new Vector3(randomX, randomY, 0f));
        return SpawnIt(numberSprite, spritePosition);
    }

    // split to allow custom spawn position
    GameObject SpawnIt(Object targetSprite, Vector3 pos) {
        GameObject sprite = Instantiate(targetSprite, pos, Quaternion.identity) as GameObject;
        sprite.GetComponent<Properties>().id = transform.childCount + 1;
        sprite.transform.SetParent(transform);
        return sprite;
    }

    public void SpawnNumber(Vector3 position, int value) {
        GameObject newNumber = SpawnIt(numberSprite, position);
        AddImpulse(newNumber);
        newNumber.GetComponent<PickupInit>().Init(value);
    }

    public void SpawnStar(Vector3 position) {
        Debug.Log("Spawning reward");
        GameObject newObj = SpawnIt(matchSprite, position);
        AddImpulse(newObj);
    }

    void AddImpulse(GameObject obj) {
        Vector3 dir = new Vector3(Random.Range(-2f, 2f), Random.Range(-2f, 2f), Random.Range(-2f, 2f));
        obj.GetComponent<Rigidbody>().AddForce(dir, ForceMode.Impulse);
    }
}
