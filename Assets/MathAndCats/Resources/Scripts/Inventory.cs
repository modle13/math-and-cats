using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Inventory : MonoBehaviour {
    private List<int> numbers = new List<int>();
    private int currentIndex = 0;

    public void Add(int val) {
        numbers.Add(val);
        SelectLast();
    }

    public String ShowAdd() {
        if (numbers.Count == 0) {
            return "";
        }
        List<string> stringNums = numbers.ConvertAll<string>(x => x.ToString());
        string joinedString = String.Join("+", stringNums.ToArray());
        Debug.Log(joinedString);
        return joinedString;
    }

    public int Sum() {
        int total = 0;
        foreach (int number in numbers) {
            total += number;
        }
        return total;
    }

    public string Show() {
        if (numbers.Count == 0) {
            return "";
        }
        List<string> stringNums = numbers.ConvertAll<string>(x => x.ToString());
        string joinedString = String.Join(",", stringNums.ToArray());
        Debug.Log(joinedString);
        return joinedString;
    }

    public int GetCount() {
        return numbers.Count;
    }

    public int GetCurrent() {
        if (numbers.Count == 0) {
            return -1;
        }
        return numbers[currentIndex];
    }

    public void SelectNext() {
        Debug.Log("SelectNext triggered");
        if (numbers.Count == 0) {
            return;
        }
        currentIndex = (currentIndex + 1) % numbers.Count;
        currentIndex = currentIndex >= numbers.Count ? 0 : currentIndex;
    }

    public void SelectPrevious() {
        Debug.Log("SelectPrevious triggered");
        if (numbers.Count == 0) {
            return;
        }
        currentIndex = (currentIndex - 1) % numbers.Count;
        currentIndex = currentIndex < 0 ? numbers.Count - 1 : currentIndex;
    }

    public void SelectLast() {
        currentIndex = numbers.Count - 1;
    }

    public int RemoveEntry() {
        if (numbers.Count == 0) {
            return -1;
        }
        int val = numbers[currentIndex];
        numbers.RemoveAt(currentIndex);
        currentIndex -= 1;
        SelectPrevious();
        return val;
    }

    public void Clear() {
        numbers = new List<int>();
    }
}
