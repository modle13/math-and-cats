using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PickupCollision : MonoBehaviour {

    Properties props;

    public void Awake() {
        props = GetComponent<Properties>();
    }

    void OnCollisionEnter(Collision other) {
        if (other.gameObject.name == "Player") {

            // setText();

            if (props.type == "producer") {
                HandleProducer();
            } else {
                SetColor();
            }
        }
    }

    void setText() {
        ObstacleTextMesh textMesh = GetComponent<ObstacleTextMesh>();
        // this is returning null; why?
        TextMeshPro tmproobj = textMesh.GetComponent<TextMeshPro>();
        tmproobj.gameObject.SetActive(true);
       // instantiate some text
    }

    void HandleProducer() {
        Debug.Log("Hit a producer: " + gameObject.name);
        if (GetComponent<ThingTimer>().GetState()) {
            Debug.Log("Got thing");
            GetComponent<ThingTimer>().ResetThing();
        }
    }

    void SetColor() {
        //Fetch the Renderer from the GameObject
        Renderer rend = GetComponent<Renderer>();
        // Set the main Color of the Material to green
        rend.material.shader = Shader.Find("_Color");
        rend.material.SetColor("_Color", Color.green);
        // // Find the Specular shader and change its Color to red
        // // need this so it's not just pink
        // rend.material.shader = Shader.Find("Specular");
        // rend.material.SetColor("_SpecColor", Color.red);
    }
}
