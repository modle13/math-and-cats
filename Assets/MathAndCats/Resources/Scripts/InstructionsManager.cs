using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class InstructionsManager : MonoBehaviour {

    public GameObject[] instructions;
    private int hideDelay = 20;

    void Start() {
        instructions = GameObject.FindGameObjectsWithTag("Instructions");
        Debug.Log("Found this many instructions objects: " + instructions.Length);
        StartCoroutine(TurnOffInstructions());
    }

    void Update () {
        if (Input.GetKeyDown("tab")) {
            Debug.Log("Toggling instructions");
            foreach (GameObject obj in instructions) {
                obj.active = !obj.active;
            }
        }
    }

    IEnumerator TurnOffInstructions() {
        yield return new WaitForSeconds(hideDelay);
        foreach (GameObject obj in instructions) {
            obj.active = false;
        }
    }
}
