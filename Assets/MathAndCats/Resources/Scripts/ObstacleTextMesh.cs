using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/* This object gets applied to the GameObject object the text should be attached to */

public class ObstacleTextMesh : MonoBehaviour {

    private bool updated;
    List<string> strings = new List<string>() {"burp", "doot", "poop", "woof", "ping", "beep", "glop"};

    // want this text to be only active when collision happens, then only for a moment

    void Update() {
        if (updated) {
            return;
        }

        GameObject text = new GameObject();
        TextMeshPro t = text.AddComponent<TextMeshPro>();
        t.name = "label";
        t.text = strings[Random.Range(0, strings.Count)];
        // t.text = transform.name;
        t.fontSize = 5;
        t.transform.localEulerAngles += new Vector3(0, 0, 90);
        t.transform.SetParent(transform);

        RectTransform rt = t.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(2, 0.5f);
        rt.localPosition = new Vector3(-1.5f, 0f, 0f);

        updated = true;
        t.gameObject.SetActive(false);
    }
}
