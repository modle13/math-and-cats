using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Can't name this Camera or it collides with TMPro scripts and complains about main not being defined
// it's likely that Camera is a preexisting script, and TMPro is doing a find on the script object 'Camera'
public class CameraController : MonoBehaviour {

  	// Update is called once per frame
	void Update () {
        Vector3 myVector = new Vector3(Player.player.transform.position.x, Player.player.transform.position.y, -3);
        transform.position = myVector;
    }
}
