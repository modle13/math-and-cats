using UnityEngine;

public class PickupInit : MonoBehaviour {

    int min = 1;
    int max = 9;

    void Awake() {
        int randomVal = Random.Range(min, max);
        Init(randomVal);
    }

    public void Init(int value) {
        GetComponent<Properties>().value = value;
        GetComponent<TextUpdater>().UpdateText(value.ToString());
    }
}
