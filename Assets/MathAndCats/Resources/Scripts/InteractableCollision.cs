using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InteractableCollision : MonoBehaviour {

    Properties props;

    public void Awake() {
        props = GetComponent<Properties>();
    }

    // this will have to be a raycast check from the player
    // because we want this to happen on proximity, not on contact
    // for the purposes of interaction with an object
    // such as a chest or door or work station
    void OnCollisionEnter(Collision other) {
        if (other.gameObject.name == "Player") {
            Debug.Log("player hit a " + name);
            other.gameObject.GetComponent<Properties>().target = gameObject;
        }
    }
}
