using UnityEngine;
using TMPro;

public class TextUpdater : MonoBehaviour {
    // http://digitalnativestudios.com/textmeshpro/docs/ScriptReference/TextMeshPro.html

    // ugh
    TextMeshPro targetTextField;
    TextMeshProUGUI targetTextFieldUGUI;

    void SetRefs() {
        targetTextField = gameObject.GetComponent<TextMeshPro>();
        targetTextFieldUGUI = gameObject.GetComponent<TextMeshProUGUI>();
    }

    public void UpdateText(string value) {
        if (targetTextField == null && targetTextFieldUGUI == null) {
            SetRefs();
        }
        if (targetTextField != null) {
            targetTextField.SetText(value);
        } else if (targetTextFieldUGUI != null) {
            targetTextFieldUGUI.SetText(value);
        } else {
            Debug.Log("No text mesh found");
        }
    }

    public void ChangeSize(int value) {
        // ugh
        if (targetTextField != null) {
            targetTextField.fontSize += value;
        } else {
            targetTextFieldUGUI.fontSize += value;
        }
    }
}
