using UnityEngine;
using TMPro;

public class Adderatorator : ActionProcessor {

    Inventory inventory;
    GameObject obstacleBucket;
    bool triggerProcessed;
    float lastTrigger;
    float delay = 0.1f;
    int maxInventoryLength = 10;
    Properties targeteratorProps;
    Properties buttonProps;
    public Properties props;
    public TextUpdater textUpdater;
    GameObject player;

    void Awake() {
        props = GetComponent<Properties>();
        textUpdater = transform.Find("Display").GetComponent<TextUpdater>();
        inventory = GetComponent<Inventory>();
        buttonProps = transform.Find("Button").transform.GetComponent<Properties>();
        obstacleBucket = GameObject.Find("ObstacleBucket");
        targeteratorProps = GameObject.Find("Targeterator").GetComponent<Properties>();
        player = GameObject.Find("Player");
    }

    void Update() {
        textUpdater.UpdateText(props.GetContent());
        CheckButton();
    }

    void CheckButton() {
        if (buttonProps.triggered && !triggerProcessed) {
            triggerProcessed = true;
            EjectNumbers("all");
            props.UpdateContent("");
        }
        if (buttonProps.idle) {
            triggerProcessed = false;
        }
    }

    public void EjectNumbers(string which) {
        Debug.Log("trying to remove " + which + " from adderator");
        if (which == "all") {
            int resultingNumber = inventory.Sum();
            Debug.Log("trying to remove " + resultingNumber + " from adderator");
            if (resultingNumber != 0) {
                inventory.Clear();
                Vector3 pos = transform.position + Vector3.up * 2;
                obstacleBucket.GetComponent<ObstacleBucket>().SpawnNumber(pos, resultingNumber);
            }
        } else if (which == "last") {
            int resultingNumber = inventory.RemoveEntry();
            Debug.Log("trying to remove " + resultingNumber + " from adderator");
            if (resultingNumber != -1) {
                player.GetComponent<Player>().Pickup(resultingNumber);
                Debug.Log("not removing " + resultingNumber);
            }
        }
        UpdateDisplay();
    }

    public override void ProcessAction(string action) {
        Debug.Log("Processing action; player is " + player);
        // return;
        if (action == "add") {
            if (!CanAdd()) {
                return;
            }
            int numberFromPlayerInventory = player.GetComponent<Inventory>().RemoveEntry();
            if (numberFromPlayerInventory > 0) {
                lastTrigger = Time.time;
                inventory.Add(numberFromPlayerInventory);
                UpdateDisplay();
            }
        } else if (action == "remove") {
            EjectNumbers("last");
        } else {
            Debug.Log("invalid action " + action);
        }
    }

    bool CanAdd() {
        bool canAdd = true;
        float elapsed = Time.time - lastTrigger;
        bool hasElapsed = elapsed > delay;
        if (!hasElapsed) {
            canAdd = false;
            Debug.Log("too soon");
        }
        if (inventory.GetCount() >= maxInventoryLength) {
            canAdd = false;
            Debug.Log("too many");
        }
        if (player.GetComponent<Inventory>().GetCurrent() + inventory.Sum() > targeteratorProps.value) {
            canAdd = false;
            Debug.Log("too big");
        }
        return canAdd;
    }

    void UpdateDisplay() {
        string addString = inventory.ShowAdd();
        string displayString = addString;
        if (inventory.GetCount() > 1) {
            int total = inventory.Sum();
            displayString = addString + "=" + total;
        }
        props.UpdateContent(displayString);
    }
}
