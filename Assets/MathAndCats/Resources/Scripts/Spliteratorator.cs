using UnityEngine;
using TMPro;

public class Spliteratorator : ActionProcessor {

    Properties props;
    // TextUpdater textUpdater;
    GameObject obstacleBucket;
    GameObject player;

    void Awake() {
        props = GetComponent<Properties>();
        // textUpdater = transform.Find("Display").GetComponent<TextUpdater>();
        obstacleBucket = GameObject.Find("ObstacleBucket");
        player = GameObject.Find("Player");
    }

    public override void ProcessAction(string action) {
        if (action != "add") {
            Debug.Log("Not processing action on Spliterator: " + action);
            return;
        }
        int removedNumber = player.gameObject.GetComponent<Inventory>().RemoveEntry();
        if (removedNumber > 0) {
            SplitNumber(removedNumber);
        }
    }

    void SplitNumber(int num) {
        Vector3 postAdjust = new Vector3(0, 3, 0);
        Vector3 pos = transform.position + postAdjust;
        int num1 = 0;
        int num2 = 0;
        if (num > 1) {
            num1 = Random.Range(1, num);
            num2 = num - num1;
        } else {
            num1 = num;
        }
        if (num1 > 0) {
            obstacleBucket.GetComponent<ObstacleBucket>().SpawnNumber(pos, num1);
        }
        if (num2 > 0) {
            obstacleBucket.GetComponent<ObstacleBucket>().SpawnNumber(pos, num2);
        }
    }
}
