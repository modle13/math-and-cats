using UnityEngine;
using TMPro;

public class Buttonerator : MonoBehaviour {

    Properties props;

    void Awake() {
        props = GetComponent<Properties>();
    }

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.name == "Player") {
            GetComponent<ThingTimer>().TriggerThing();
        }
    }

}
