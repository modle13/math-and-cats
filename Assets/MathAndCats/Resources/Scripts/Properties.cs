using UnityEngine;
using System;
using TMPro;

public class Properties : MonoBehaviour {
    public string type;
    public string produces;
    public string job;
    public string baseJob;
    public bool targeted;
    public GameObject target;

    public bool ready;
    public bool triggered;
    public bool active;
    public bool idle;

    public bool engaged;
    public int targetedBy = 0;
    public bool selected;
    public bool destructable;
    public bool workable;
    public int id;
    public string currentState;
    private int score;
    public int value;
    private string content;

    public void UpdateValue(int newValue) {
        value = newValue;
        string contentString = newValue > 0 ? newValue.ToString() : "";
        UpdateContent(contentString);
        UpdateDisplay();
    }

    public int GetValue() {
        return value;
    }

    public void AddToValue(int valToAdd) {
        value = value + valToAdd;
        UpdateContent(value.ToString());
        UpdateDisplay();
    }

    public void UpdateContent(string newValue) {
        content = newValue;
    }

    public string GetContent() {
        return content;
    }

    public void UpdateDisplay() {
        gameObject.GetComponent<TextUpdater>().UpdateText(content);
    }
}
