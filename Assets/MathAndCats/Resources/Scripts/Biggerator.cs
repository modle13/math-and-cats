using UnityEngine;
using TMPro;

public class Biggerator : MonoBehaviour {

    Properties props;
    Transform obstacleBucket;
    public int sizeDelta = 1;
    public int maxSize = 5;
    public int timesBiggerated = 0;
    // this needs to exist because text and collider do not scale 1:1
    // so sizeDelta needs to be modified after being applied to the text before it is applied to the collider
    public float colliderDivisor = 4.0f;
    bool triggerProcessed;

    void Awake() {
        props = GetComponent<Properties>();
        obstacleBucket = GameObject.Find("ObstacleBucket").transform;
    }

    void Update() {
        CheckButton();
    }

    void CheckButton() {
        if (props.triggered && !triggerProcessed) {
            triggerProcessed = true;
            UpdateObstacles();
        }
        if (props.idle) {
            triggerProcessed = false;
        }
    }

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.name == "Player") {
            GetComponent<ThingTimer>().TriggerThing();
        }
    }

    void UpdateObstacles() {
        // this will limit how large numbers can get
        if (timesBiggerated > maxSize) {
            return;
        }
        foreach (Transform child in obstacleBucket) {
            // FIXME: this TextUpdater is changing the size of the text too fast
            child.GetComponent<TextUpdater>().ChangeSize(sizeDelta);
            // TODO: need to map font size to corresponding collider x,y
            // then read font size, and get size value for collider
            // to tie them together explicitly

            // FIXME: this doesn't actually seem to be updating the collider
            BoxCollider boxCollider = child.GetComponent<BoxCollider>();
            float colliderDim = sizeDelta / colliderDivisor;
            Debug.Log(colliderDim + " is the scale factor");

            Vector3 vectorToAdd = new Vector3(colliderDim, colliderDim, colliderDim);
            // Vector3 vectorToAdd = new Vector3(sizeDelta, sizeDelta, sizeDelta);
            boxCollider.size = boxCollider.size + vectorToAdd;
        }
        timesBiggerated += 1;
    }
}
