using UnityEngine;
using TMPro;

public class Targeteratorator : ActionProcessor {

    Properties props;
    TextUpdater textUpdater;
    GameObject obstacleBucket;
    int max = 19;
    int min = 5;
    GameObject player;

    void Awake() {
        props = GetComponent<Properties>();
        textUpdater = transform.Find("Display").GetComponent<TextUpdater>();
        obstacleBucket = GameObject.Find("ObstacleBucket");
        player = GameObject.Find("Player");
    }

    void Update() {
        textUpdater.UpdateText(props.value.ToString());
    }

    public override void ProcessAction(string action) {
        if (action != "add") {
            Debug.Log("Not processing action on Targeterator: " + action);
            return;
        }
        CheckNumber();
    }

    void CheckNumber() {
        int currentNumber = player.GetComponent<Inventory>().GetCurrent();
        if (props.value == currentNumber) {
            Debug.Log("Wuahh!");
            MakeReward();
            player.GetComponent<Inventory>().RemoveEntry();
            GenerateNumber();
        }
    }

    void GenerateNumber() {
        props.value = Random.Range(min, max);
    }

    void MakeReward() {
        Vector3 postAdjust = new Vector3(0, 3, 0);
        Vector3 pos = transform.position + postAdjust;
        obstacleBucket.GetComponent<ObstacleBucket>().SpawnStar(pos);
    }
}
