using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mover : MonoBehaviour {

    private Rigidbody rigidBody;
    int brakeMultiplier = 2;

    void Awake() {
        rigidBody = GetComponent<Rigidbody>();
    }

	void Update () {
        SlowItDown();
    }

    void SlowItDown() {
        Vector2 moveForce = new Vector2(-rigidBody.velocity.x * brakeMultiplier, -rigidBody.velocity.y * brakeMultiplier);
        rigidBody.AddForce(moveForce);
    }
}
